/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef ECS_UTILS_H
#define ECS_UTILS_H

#include "ecs.h"

namespace ecs {
	class BinarySystem : public System {
		private:
			class InnerSystem : public System {
				private:
					BinarySystem *outer;
					void update(Entity);
				public:
					InnerSystem(BinarySystem*);
					friend class BinarySystem;
			};
			InnerSystem inner;
			Entity outerEntity;

			void update(Entity);
		protected:
			virtual void update(Entity, Entity) = 0;
			template <typename T> void require();
			template <typename T> void requireA();
			template <typename T> void requireB();

			template <typename T> void exclude();
			template <typename T> void excludeA();
			template <typename T> void excludeB();
		public:
			BinarySystem();
			void init(World*);
	};
	template <typename T> void BinarySystem::require() {
		requireA<T>();
		requireB<T>();
	}
	template <typename T> void BinarySystem::requireA() {
		System::require<T>();
	}
	template <typename T> void BinarySystem::requireB() {
		inner.require<T>();
	}
	template <typename T> void BinarySystem::exclude() {
		excludeA<T>();
		excludeB<T>();
	}
	template <typename T> void BinarySystem::excludeA() {
		System::exclude<T>();
	}
	template <typename T> void BinarySystem::excludeB() {
		inner.exclude<T>();
	}
}

#endif
