/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef ECS_VECTOR_H
#define ECS_VECTOR_H

#include <vector>

namespace ecs {
	template <typename T>
	class Vector {
		private:
			std::vector<T> values;
			int size;
		public:
			Vector();
			int getSize();
			void clear();
			T& operator [] (int);
	};
	template <typename T> Vector<T>::Vector() : size(0) {
	}
	template <typename T> int Vector<T>::getSize() {
		return size;
	}
	template <typename T> void Vector<T>::clear() {
		values.clear();
		size = 0;
	}
	template <typename T> T& Vector<T>::operator [] (int index) {
		if(index >= size) {
			size = index + 1;
			values.resize(size);
		}
		return values[index];
	}
};

#endif
