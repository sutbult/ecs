/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef ECS_H
#define ECS_H

#include "ecs_multivector.h"
#include "ecs_bitset.h"
#include "ecs_vector.h"
#include <stdint.h>
#include <bitset>

#define __ECS_TYPEDEFS \
typedef uint32_t EntityID;\
typedef unsigned int ComponentID;\
typedef uint64_t WorldState;\
typedef void (*ComponentDeleter)(void*);

namespace ecs {
	class World;
	class System;
	class Entity {
		private:
			__ECS_TYPEDEFS
			static unsigned int typeIDCounter;
			EntityID id;
			World *world;

			Entity(EntityID, World*);
			template <typename T> static unsigned int getTypeID();
			template <typename T> static void componentDeleter(void*);
		public:
			Entity();
			bool valid();
			void destroy();
			template <typename T> T& get();
			template <typename T> Entity set(T);
			template <typename T> bool has();
			template <typename T> Entity unset();

			bool operator == (Entity&);
			bool operator != (Entity&);
			friend class World;
			friend class System;
	};
	class World {
		private:
			__ECS_TYPEDEFS

			// Components
			Multivector components;
			Vector<ComponentDeleter> componentDeleters;
			Vector<Bitset> masks;

			// Entities
			Bitset usedEntities;
			EntityID firstFreeID;
			EntityID lastFreeID;
			Entity globalEntity;

			// Systems
			WorldState state;
			Vector<System*> systems;

			// Components
			void* getComponent(EntityID, ComponentID);
			void setComponent(void*, EntityID, ComponentID, ComponentDeleter);
			bool hasComponent(EntityID, ComponentID);
			bool unsetComponent(EntityID, ComponentID);

			// Entities
			void destroyEntity(EntityID);
			int allocEntity();

			// Systems
			void newState();
			void addSystem(System*);
		public:
			World();
			~World();

			// Entities
			Entity create();
			Entity* create(int, Entity* = 0);
			Entity global();

			// Systems
			template <typename T> void add();
			void processAll();

			friend class Entity;
			friend class System;
	};
	class System {
		private:
			__ECS_TYPEDEFS
			World *world;
			Bitset excluded;
			Bitset required;
			WorldState worldState;
			Vector<EntityID> entityCache;
			bool cleanCache;

			void requireComponent(ComponentID);
			void excludeComponent(ComponentID);
			void processCached();
			void processCalculated();
		protected:
			virtual void begin();
			virtual void end();
			virtual void update(Entity) = 0;
			template <typename T> void require();
			template <typename T> void exclude();
		public:
			System();
			virtual ~System();
			void process();
			World* getWorld();
			virtual void init(World*);
			void init(World&);
			Entity global();
	};

	template <typename T> T& Entity::get() {
		return *(T*)world->getComponent(id, getTypeID<T>());
	}
	template <typename T> Entity Entity::set(T component) {
		if(world) {
			world->setComponent(new T(component), id, getTypeID<T>(), componentDeleter<T>);
		}
		return *this;
	}
	template <typename T> bool Entity::has() {
		return world && world->hasComponent(id, getTypeID<T>());
	}
	template <typename T> Entity Entity::unset() {
		if(world) {
			world->unsetComponent(id, getTypeID<T>());
		}
		return *this;
	}
	template <typename T> unsigned int Entity::getTypeID() {
		static unsigned int typeID = typeIDCounter++;
		return typeID;
	}
	template <typename T> void Entity::componentDeleter(void *component) {
		delete (T*)component;
	}
	template <typename T> void World::add() {
		addSystem(new T());
	}
	template <typename T> void System::require() {
		requireComponent(Entity::getTypeID<T>());
	}
	template <typename T> void System::exclude() {
		excludeComponent(Entity::getTypeID<T>());
	}
};
#undef __ECS_TYPEDEFS

#endif
