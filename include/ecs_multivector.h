/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef ECS_MULTIVECTOR_H
#define ECS_MULTIVECTOR_H

#include <stdint.h>

namespace ecs {
	class Multivector {
		private:
			typedef uint32_t Index;
			void **values;
			Index xSize;
			Index ySize;
			Index xCapacity;
			Index yCapacity;

			void checkSize(Index, Index);
			void ensureCapacity(Index, Index);
			void enlarge(Index, Index);

			void correctSize(Index, Index*);
			Index correctCapacity(Index, Index);

			Multivector(const Multivector&);
			Multivector* operator = (Multivector);
		public:
			Multivector();
			~Multivector();
			void* get(Index, Index);
			void set(void*, Index, Index);

			Index getXSize();
			Index getYSize();
			Index getXCapacity();
			Index getYCapacity();
	};
};

#endif
