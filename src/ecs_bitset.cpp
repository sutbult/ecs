/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "ecs_bitset.h"
#include <algorithm>

using namespace ecs;

Bitset::Bitset() : intCount(0), bitCount(0), bits(0) {
}
Bitset::~Bitset() {
	if(bits) {
		delete[] bits;
	}
}
Bitset::Bitset(const Bitset &other) : intCount(0), bitCount(other.bitCount), bits(0) {
	enlarge(other.intCount);
	for(int i = 0; i < other.intCount; i++) {
		bits[i] = other.bits[i];
	}
}
Bitset& Bitset::operator = (Bitset other) {
	std::swap(intCount, other.intCount);
	std::swap(bitCount, other.bitCount);
	std::swap(bits, other.bits);
	return *this;
}

void Bitset::ensureCapacity(BitsetIndex index) {
	if(index >= capacity()) {
		int newIntCount = intCount ? intCount : 1;
		while(index >= newIntCount * BITSET_INT_BITS) {
			newIntCount *= 2;
		}
		enlarge(newIntCount);
	}
}
void Bitset::enlarge(int newIntCount) {
	BitsetInt *newBits = new BitsetInt[newIntCount];

	for(int i = 0; i < intCount; i++) {
		newBits[i] = bits[i];
	}
	for(int i = intCount; i < newIntCount; i++) {
		newBits[i] = 0;
	}
	if(bits) {
		delete[] bits;
	}
	bits = newBits;
	intCount = newIntCount;
}
void Bitset::checkSize(bool value, BitsetIndex index) {
	if(value) {
		if(index >= bitCount) {
			bitCount = index + 1;
		}
	}
	else {
		if(index == bitCount - 1) {
			shrink();
		}
	}
}
void Bitset::shrink() {
	while(bitCount > 0 && !get(bitCount - 1)) {
		bitCount--;
	}
}

#define indexToByte(index) ((index) / BITSET_INT_BITS)
#define indexToBit(index) ((index) % BITSET_INT_BITS)
#define indexToBitmask(index) (1 << indexToBit(index))

bool Bitset::get(BitsetIndex index) const {
	if(index < size()) {
		return !!(bits[indexToByte(index)] & indexToBitmask(index));
	}
	else {
		return 0;
	}
}
void Bitset::set(bool value, BitsetIndex index) {
	ensureCapacity(index);

	if(value) {
		bits[indexToByte(index)] |= indexToBitmask(index);
	}
	else {
		bits[indexToByte(index)] &= ~indexToBitmask(index);
	}
	checkSize(value, index);
}
BitsetIndex Bitset::capacity() const {
	return intCount * BITSET_INT_BITS;
}
BitsetIndex Bitset::size() const {
	return bitCount;
}
bool Bitset::operator == (const Bitset &other) {
	if(size() != other.size()) {
		return false;
	}
	int count = std::min(intCount, other.intCount);
	for(int i = 0; i < count; i++) {
		if(bits[i] != other.bits[i]) {
			return false;
		}
	}
	return true;
}
bool Bitset::operator != (const Bitset &other) {
	return !(*this == other);
}
Bitset Bitset::operator | (const Bitset &other) {
	int count = std::min(intCount, other.intCount);
	int overCount = std::max(intCount, other.intCount);

	Bitset bitset;
	bitset.enlarge(overCount);
	for(int i = 0; i < count; i++) {
		bitset.bits[i] = bits[i] | other.bits[i];
	}
	for(int i = count; i < intCount; i++) {
		bitset.bits[i] = bits[i];
	}
	for(int i = count; i < other.intCount; i++) {
		bitset.bits[i] = other.bits[i];
	}
	bitset.bitCount = std::max(size(), other.size());
	return bitset;
}
Bitset Bitset::operator & (const Bitset &other) {
	int count = std::min(intCount, other.intCount);

	Bitset bitset;
	bitset.enlarge(count);
	for(int i = 0; i < count; i++) {
		bitset.bits[i] = bits[i] & other.bits[i];
	}
	bitset.bitCount = std::min(size(), other.size());
	bitset.shrink();
	return bitset;
}

Bitset::Reference Bitset::operator [] (BitsetIndex index) {
	return Reference(index, this);
}
Bitset::Reference::Reference(BitsetIndex index, Bitset *bitset) : index(index), bitset(bitset) {
}
bool Bitset::Reference::operator = (bool value) {
	bitset->set(value, index);
	return value;
}
Bitset::Reference::operator bool() const {
	return bitset->get(index);
}

std::ostream& ecs::operator << (std::ostream &os, const Bitset &bitset) {
	BitsetIndex size = bitset.size();
	for(BitsetIndex i = 0; i < size; i++) {
		os << bitset.get(size - i - 1);
	}
	if(bitset.size() == 0) {
		os << "[empty bitset]";
	}
	return os;
}
