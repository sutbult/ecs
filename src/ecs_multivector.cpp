/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "ecs_multivector.h"
#include <string.h>
#include <algorithm>

using namespace ecs;

Multivector::Multivector() :
	values(new void*[1]),
	xSize(1),
	ySize(1),
	xCapacity(1),
	yCapacity(1) {
	values[0] = 0;
}
Multivector::~Multivector() {
	if(values) {
		delete[] values;
	}
}

#define toIndexStride(x, y, yStride) ((yStride) * (x) + (y))
#define toIndex(x, y) toIndexStride(x, y, yCapacity)

void Multivector::correctSize(Index i, Index *size) {
	if(i >= *size) {
		*size = i + 1;
	}
}
void Multivector::checkSize(Index x, Index y) {
	if(x >= xSize || y >= ySize) {
		ensureCapacity(x, y);
		correctSize(x, &xSize);
		correctSize(y, &ySize);
	}
}
Multivector::Index Multivector::correctCapacity(Index i, Index capacity) {
	while(i >= capacity) {
		capacity *= 2;
	}
	return capacity;
}
void Multivector::ensureCapacity(Index x, Index y) {
	if(x >= xCapacity || y >= yCapacity) {
		Index newXCapacity = correctCapacity(x, xCapacity);
		Index newYCapacity = correctCapacity(y, yCapacity);
		enlarge(newXCapacity, newYCapacity);
	}
}
void Multivector::enlarge(Index newXCapacity, Index newYCapacity) {
	void **newValues = new void*[newXCapacity * newYCapacity];

	if(yCapacity != newYCapacity) {
		for(Index x = 0; x < xCapacity; x++) {
			void **newValueRow = newValues + x * newYCapacity;

			for(Index y = 0; y < yCapacity; y++) {
				newValueRow[y] = values[toIndex(x, y)];
			}
			memset(newValueRow + yCapacity, 0,
				(newYCapacity - yCapacity) * sizeof(void*));
		}
	}
	else {
		for(Index i = 0; i < xCapacity * yCapacity; i++) {
			newValues[i] = values[i];
		}
	}
	if(xCapacity != newXCapacity) {
		memset(newValues + xCapacity * newYCapacity, 0,
			(newXCapacity - xCapacity) * newYCapacity * sizeof(void*));
	}

	delete[] values;
	values = newValues;
	xCapacity = newXCapacity;
	yCapacity = newYCapacity;
}
void Multivector::set(void *value, Index x, Index y) {
	checkSize(x, y);
	values[toIndex(x, y)] = value;
}
void* Multivector::get(Index x, Index y) {
	if(x < xSize && y < ySize) {
		return values[toIndex(x, y)];
	}
	else {
		return 0;
	}
}
Multivector::Index Multivector::getXSize() {
	return xSize;
}
Multivector::Index Multivector::getYSize() {
	return ySize;
}
Multivector::Index Multivector::getXCapacity() {
	return xCapacity;
}
Multivector::Index Multivector::getYCapacity() {
	return yCapacity;
}
