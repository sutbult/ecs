/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "ecs.h"

using namespace ecs;

unsigned int Entity::typeIDCounter = 0;

Entity::Entity(EntityID id, World *world) : id(id), world(world) {
}
Entity::Entity() : id(0), world(0) {
}
bool Entity::valid() {
	return world && world->usedEntities[id];
}
void Entity::destroy() {
	if(valid()) {
		world->destroyEntity(id);
	}
}
bool Entity::operator == (Entity &other) {
	return id == other.id && world == other.world;
}
bool Entity::operator != (Entity &other) {
	return !(*this == other);
}

World::World() : firstFreeID(0), lastFreeID(0), state(0) {
	globalEntity = create();
}
World::~World() {
	for(EntityID e = 0; e < components.getXSize(); e++) {
		for(ComponentID cid = 0; cid < components.getYSize(); cid++) {
			void *component = components.get(e, cid);
			if(component) {
				componentDeleters[cid](component);
			}
		}
	}
	for(unsigned int i = 0; i < systems.getSize(); i++) {
		delete systems[i];
	}
}
Entity World::create() {
	EntityID id = allocEntity();
	usedEntities[id] = 1;
	newState();
	return Entity(id, this);
}
Entity* World::create(int count, Entity *entities) {
	if(!entities) {
		entities = new Entity[count];
	}
	for(int i = 0; i < count; i++) {
		entities[i] = create();
	}
	return entities;
}
void* World::getComponent(EntityID e, ComponentID cid) {
	return components.get(e, cid);
}
void World::setComponent(void *c, EntityID e, ComponentID cid, ComponentDeleter deleter) {
	if(!unsetComponent(e, cid)) {
		componentDeleters[cid] = deleter;
	}
	components.set(c, e, cid);
	masks[e][cid] = 1;
	newState();
}
bool World::hasComponent(EntityID e, ComponentID cid) {
	return masks[e][cid];
}
bool World::unsetComponent(EntityID e, ComponentID cid) {
	void *component = components.get(e, cid);
	if(component) {
		masks[e][cid] = 0;
		componentDeleters[cid](component);
		components.set(0, e, cid);
		newState();
	}
	return !!component;
}
void World::destroyEntity(EntityID e) {
	usedEntities[e] = 0;
	masks[e] = Bitset();
	if(e < firstFreeID) {
		firstFreeID = e;
	}
}
int World::allocEntity() {
	EntityID id;
	if(firstFreeID >= lastFreeID) {
		id = lastFreeID++;
		firstFreeID = lastFreeID;
	}
	else {
		id = firstFreeID++;
		while(firstFreeID < lastFreeID && usedEntities[firstFreeID]) {
			firstFreeID++;
		}
	}
	return id;
}
void World::newState() {
	state++;
}
void World::addSystem(System *system) {
	system->init(this);
	systems[systems.getSize()] = system;
}
void World::processAll() {
	unsigned int size = systems.getSize();
	for(unsigned int i = 0; i < size; i++) {
		systems[i]->process();
	}
}
Entity World::global() {
	return globalEntity;
}

System::System() : worldState(0), cleanCache(true) {
}
System::~System() {
}
void System::requireComponent(ComponentID id) {
	required[id] = 1;
	cleanCache = true;
}
void System::excludeComponent(ComponentID id) {
	excluded[id] = 1;
	cleanCache = true;
}
void System::process() {
	begin();
	if(cleanCache || worldState != world->state) {
		processCalculated();
	}
	else {
		processCached();
	}
	end();
}
void System::processCached() {
	int size = entityCache.getSize();
	for(int i = 0; i < size; i++) {
		update(Entity(entityCache[i], world));
	}
}
void System::processCalculated() {
	cleanCache = false;
	worldState = world->state;
	entityCache.clear();

	for(EntityID e = 0; e < world->lastFreeID; e++) {
		if(
			(world->masks[e] & required) == required &&
			(world->masks[e] & excluded).size() == 0
		) {
			update(Entity(e, world));
			entityCache[entityCache.getSize()] = e;
		}
	}
}
void System::begin() {
}
void System::end() {
}
World* System::getWorld() {
	return world;
}
void System::init(World *world) {
	this->world = world;
}
void System::init(World &world) {
	init(&world);
}
Entity System::global() {
	return getWorld()->global();
}
