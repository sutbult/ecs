/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "catch.hpp"
#include "ecs.h"

using namespace ecs;

/*
Performance test inspiration:
http://tilemapkit.com/2015/10/entity-component-systems-compared-benchmarked-entityx-anax-artemis/
http://archive.fo/tjuMP
*/

struct Position {
	float x;
	float y;
	Position(float x, float y) : x(x), y(y) {}
};
struct Velocity {
	float x;
	float y;
	Velocity(float x, float y) : x(x), y(y) {}
};
struct Conflabulation {
	float x;
	float y;
	Conflabulation(float x, float y) : x(x), y(y) {}
};

class Mover : public System {
	public:
		Mover();
	private:
		void update(Entity);
};
class Conflabulator : public System {
	public:
		Conflabulator();
	private:
		void update(Entity);
};

Mover::Mover() {
	require<Position>();
	require<Velocity>();
}
void Mover::update(Entity e) {
	Position &position = e.get<Position>();
	Velocity &velocity = e.get<Velocity>();
	position.x += velocity.x;
	position.y += velocity.y;
}
Conflabulator::Conflabulator() {
	require<Position>();
	require<Conflabulation>();
}
void Conflabulator::update(Entity e) {
	Position &position  = e.get<Position>();
	Conflabulation &conflabulation = e.get<Conflabulation>();

	#define RAND_INT (rand() & ((1 << 16) - 1))
	#define RAND_FLOAT ((float)RAND_INT / (float)(1 << 15) - 1)
	position.x += RAND_FLOAT * conflabulation.x;
	position.y += RAND_FLOAT * conflabulation.y;
}

void performTest(int entityCount, int iterationCount = 1000) {
	World world;
	world.add<Mover>();
	world.add<Conflabulator>();

	for(int i = 0; i < entityCount; i++) {
		Entity e = world.create();
		e.set(Position(0, 0));
		e.set(Velocity(1, 1));
		if(i % 2) {
			e.set(Conflabulation(0.25f, 0.5f));
		}
	}
	for(int i = 0; i < iterationCount; i++) {
		world.processAll();
	}
}


TEST_CASE("ECS performance tests", "[ecs][.][performance]") {
	SECTION("25 entities") {
		performTest(25);
	}
	SECTION("50 entities") {
		performTest(50);
	}
	SECTION("100 entities") {
		performTest(100);
	}
	SECTION("200 entities") {
		performTest(200);
	}
	SECTION("400 entities") {
		performTest(400);
	}
	SECTION("800 entities") {
		performTest(800);
	}
	SECTION("1600 entities") {
		performTest(1600);
	}
	SECTION("3200 entities") {
		performTest(3200);
	}
	SECTION("6400 entities") {
		performTest(6400);
	}
	SECTION("12800 entities") {
		performTest(12800);
	}
	SECTION("25600 entities") {
		performTest(25600);
	}
}
