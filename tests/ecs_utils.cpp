/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "catch.hpp"
#include "ecs_utils.h"

using namespace ecs;

class TestSpeed {
	public:
		int s;
		TestSpeed(int s) : s(s) {}
};

class TestBinarySystem : public BinarySystem {
	public:
		TestBinarySystem();
	private:
		void update(Entity, Entity);
};
TestBinarySystem::TestBinarySystem() {
	requireA<int>();
	requireB<TestSpeed>();

	exclude<char>();
}
void TestBinarySystem::update(Entity a, Entity b) {
	int &i = a.get<int>();
	TestSpeed &speed = b.get<TestSpeed>();
	i += speed.s;
}

int recSum(int n) {
	return n > 0 ? n + recSum(n - 1) : 0;
}
const int A_COUNT = 100;
const int B_COUNT = A_COUNT;
const int B_SUM = recSum(B_COUNT);

TEST_CASE("Binary system works", "[ecs][utils][binary_system]") {
	World world;
	world.add<TestBinarySystem>();

	SECTION("One entity A") {
		Entity a = world.create();
		a.set<int>(5);

		SECTION("One entity B") {
			Entity b = world.create();
			b.set(TestSpeed(2));

			world.processAll();
			REQUIRE(a.get<int>() == 7);
		}
		SECTION("Multiple entities B") {
			for(int i = 0; i < B_COUNT; i++) {
				Entity b = world.create();
				b.set(TestSpeed(i + 1));
			}
			world.processAll();
			REQUIRE(a.get<int>() == 5 + B_SUM);
		}
	}
	SECTION("Multiple entities A") {
		Entity as[A_COUNT];
		world.create(A_COUNT, as);

		for(int i = 0; i < A_COUNT; i++) {
			as[i].set<int>(i);
		}
		SECTION("One entity B") {
			Entity b = world.create();
			b.set(TestSpeed(2));
			world.processAll();

			for(int i = 0; i < A_COUNT; i++) {
				REQUIRE(as[i].get<int>() == i + 2);
			}
		}
		SECTION("Multiple entities B") {
			for(int i = 0; i < B_COUNT; i++) {
				Entity b = world.create();
				b.set(TestSpeed(i + 1));
			}
			world.processAll();

			for(int i = 0; i < A_COUNT; i++) {
				REQUIRE(as[i].get<int>() == i + B_SUM);
			}
		}
	}
	SECTION("Exclusion works") {
		Entity a = world.create();
		Entity b = world.create();

		SECTION("Entity A exclude") {
			a.set<int>(4);
			b.set('b');
			b.set(TestSpeed(3));

			world.processAll();
			REQUIRE(a.get<int>() == 4);
		}
		SECTION("Entity B exclude") {
			a.set('a');
			a.set<int>(4);
			b.set(TestSpeed(3));

			world.processAll();
			REQUIRE(a.get<int>() == 4);
		}
	}
}
