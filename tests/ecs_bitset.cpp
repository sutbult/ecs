/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "catch.hpp"
#include "ecs_bitset.h"
#include <string.h>

using namespace ecs;

class BoolArray {
	public:
		bool *arr;
		BoolArray(int);
		~BoolArray();
};
BoolArray::BoolArray(int length) : arr(new bool[length]) {
	memset(arr, 0, length);
}
BoolArray::~BoolArray() {
	delete[] arr;
}

TEST_CASE("Bitset holds bits", "[ecs][bitset]") {
	Bitset bitset;
	SECTION("First index has zero as default value") {
		REQUIRE_FALSE(bitset.get(0));

		SECTION("Random index has zero as default value") {
			REQUIRE_FALSE(bitset.get(rand()));

			SECTION("Random indices has zero as default values") {
				for(int i = 0; i < 1000; i++) {
					REQUIRE_FALSE(bitset.get(rand()));
				}
			}
		}
	}
	SECTION("First index holds values") {
		bitset.set(1, 0);
		REQUIRE(bitset.get(0));

		bitset.set(0, 0);
		REQUIRE_FALSE(bitset.get(0));

		SECTION("First index can be accessed by array operator") {
			REQUIRE_FALSE(bitset[0]);

			SECTION("First index can be assigned by array operator") {
				bitset[0] = 1;
				REQUIRE(bitset[0]);
			}
			SECTION("First and second index can be assigned simultaneously by array operator") {
				bitset[0] = bitset[1] = 1;
				REQUIRE(bitset[0]);
				REQUIRE(bitset[1]);

				bitset[0] = bitset[1] = 0;
				REQUIRE_FALSE(bitset[0]);
				REQUIRE_FALSE(bitset[1]);
			}
		}
		SECTION("Second index holds values simultaneously") {
			bitset.set(1, 1);
			REQUIRE(bitset.get(1));
			REQUIRE_FALSE(bitset.get(0));

			SECTION("Both can be accessed by array operator") {
				REQUIRE(bitset[1]);
				REQUIRE_FALSE(bitset[0]);
			}
		}
	}
	SECTION("Larger index holds value and enlarges the bitset") {
		const int TEST_INDEX = BITSET_INT_BITS + 2;
		BitsetIndex capacity = bitset.capacity();
		bitset.set(1, TEST_INDEX);

		REQUIRE(bitset.get(TEST_INDEX));
		REQUIRE(bitset.capacity() > capacity);
	}
	SECTION("Large number of continuous indices holds values") {
		const int INDEX_COUNT = 100;
		#define INDEX_LOOP for(int i = 0; i < INDEX_COUNT; i++)
		#define INDEX_VALUE ((i % 2) != 0)

		INDEX_LOOP {
			bitset.set(INDEX_VALUE, i);
		}
		INDEX_LOOP {
			REQUIRE(bitset.get(i) == INDEX_VALUE);
		}
		SECTION("Using reverse pattern") {
			INDEX_LOOP {
				bitset.set(!INDEX_VALUE, i);
			}
			INDEX_LOOP {
				REQUIRE_FALSE(bitset.get(i) == INDEX_VALUE);
			}
		}
		SECTION("Using array operator") {
			INDEX_LOOP {
				bitset[i] = INDEX_VALUE;
			}
			INDEX_LOOP {
				REQUIRE(bitset[i] == INDEX_VALUE);
				REQUIRE_FALSE(bitset[i] != INDEX_VALUE);
				REQUIRE_FALSE((!bitset[i]) == INDEX_VALUE);
			}
		}
		#undef INDEX_LOOP
		#undef INDEX_VALUE
	}
	#define INDEX_VALUE ((rand() % 2) != 0)
	SECTION("Large number of continuous indices holds random values") {
		#define INDEX_LOOP for(int i = 0; i < indexCount; i++)

		for(int run = 1; run <= 10; run++) {
			int indexCount = run * 100;
			BoolArray vals(indexCount);

			INDEX_LOOP {
				bool val = INDEX_VALUE;
				vals.arr[i] = val;
				bitset.set(val, i);
			}
			INDEX_LOOP {
				REQUIRE(bitset.get(i) == vals.arr[i]);
			}
		}
		#undef INDEX_LOOP
	}
	SECTION("Large number of random indices holds random values") {
		int INDEX_COUNT = 1000;
		int INDEX_RANGE = 10000;
		BoolArray hasValue(INDEX_RANGE);
		BoolArray values(INDEX_RANGE);

		for(int i = 0; i < INDEX_COUNT; i++) {
			int index;
			do {
				index = rand() % INDEX_RANGE;
			} while(hasValue.arr[index]);
			hasValue.arr[index] = 1;

			bool value = INDEX_VALUE;
			bitset.set(value, index);
			values.arr[index] = value;
		}
		for(int i = 0; i < INDEX_RANGE; i++) {
			if(hasValue.arr[i]) {
				INFO(
					"Index: " << i << " " <<
					"Bitset: " << bitset.get(i) << " " <<
					"Value: " << values.arr[i]
				);
				REQUIRE(bitset.get(i) == values.arr[i]);
			}
		}
	}
	#undef INDEX_VALUE
}
TEST_CASE("Bitset size are correct", "[ecs][bitset][size]") {
	Bitset bitset;

	SECTION("First index works") {
		bitset.set(1, 0);
		REQUIRE(bitset.size() == 1);

		SECTION("Size resets for zero values") {
			bitset.set(0, 0);
			REQUIRE(bitset.size() == 0);
		}
		SECTION("Larger index works") {
			bitset.set(1, 9);
			REQUIRE(bitset.size() == 10);

			SECTION("Size falls back to first index when set to zero") {
				bitset.set(0, 9);
				REQUIRE(bitset.size() == 1);
			}
			SECTION("Middle index doesn't change size") {
				bitset.set(1, 5);
				REQUIRE(bitset.size() == 10);

				SECTION("Size changes to middle index when set to zero") {
					bitset.set(0, 9);
					REQUIRE(bitset.size() == 6);
				}
			}
		}
	}
	SECTION("Large number of continuous indices gives correct size") {
		#define even(i) ((i) / 2) * 2

		for(int i = 0; i < 100; i++) {
			int expectedSize = even(i + 1);
			INFO(
				"Index: " << i << " " <<
				"Value: " << i % 2 << " " <<
				"Expected size: " << expectedSize
			);
			bitset.set(i % 2 != 0, i);
			INFO("Actual size: " << bitset.size());
			REQUIRE(bitset.size() == expectedSize);
		}
		SECTION("Reversing the assignment reverts the size") {
			for(int i = 99; i >= 0; i--) {
				int expectedSize = even(i);
				INFO(
					"Index: " << i << " " <<
					"Value: " << i % 2 << " " <<
					"Expected size: " << expectedSize
				);
				bitset.set(0, i);
				INFO("Actual size: " << bitset.size());
				REQUIRE(bitset.size() == expectedSize);
			}
		}
		SECTION("Assigning the first half to zero doesn't change the size") {
			for(int i = 0; i < 50; i++) {
				bitset.set(0, i);
				REQUIRE(bitset.size() == 100);
			}
			SECTION("Reversing the assignment for the second half reverts the size") {
				for(int i = 99; i >= 52; i--) {
					bitset.set(0, i);
					REQUIRE(bitset.size() == even(i));
				}

				SECTION("Switching the last index sets the size to zero") {
					bitset.set(0, 51);
					REQUIRE(bitset.size() == 0);
				}
			}
		}
		#undef even
	}
}
TEST_CASE("Bitsets can be copied and be assigned", "[ecs][bitset]") {
	const int INDEX_COUNT = BITSET_INT_BITS * 2 + 1;
	#define INDEX_LOOP for(int i = 0; i < INDEX_COUNT; i++)
	#define INDEX_VALUE ((i % 2) != 0)

	Bitset bitset;
	INDEX_LOOP {
		bitset.set(INDEX_VALUE, i);
	}
	SECTION("Bitsets can be copied") {
		Bitset bitset2 = bitset;
		INDEX_LOOP {
			REQUIRE(bitset2.get(i) == INDEX_VALUE);
		}
	}
	SECTION("Bitsets can be assigned") {
		Bitset bitset2;
		bitset2 = bitset;
		INDEX_LOOP {
			REQUIRE(bitset2.get(i) == INDEX_VALUE);
		}
	}
	#undef INDEX_LOOP
	#undef INDEX_VALUE
}
TEST_CASE("Bitsets can be compared", "[ecs][bitset][compare]") {
	const int INDEX_COUNT = BITSET_INT_BITS * 2 + 1;
	const int INDEX_LARGE = BITSET_INT_BITS * 5;

	Bitset bitset[4];
	for(int i = 0; i < INDEX_COUNT; i++) {
		bitset[0].set(i % 2 != 0, i);
	}
	for(int b = 1; b < sizeof(bitset) / sizeof(bitset[0]); b++) {
		bitset[b] = bitset[0];
	}
	bitset[2].set(1, 0);
	bitset[3].set(1, INDEX_LARGE);

	SECTION("Equal bitsets are equal") {
		REQUIRE(bitset[0] == bitset[1]);

		SECTION("Unequality operator works too") {
			REQUIRE_FALSE(bitset[0] != bitset[1]);
		}
	}
	SECTION("Unequal bitsets are not equal") {
		REQUIRE_FALSE(bitset[0] == bitset[2]);
		REQUIRE_FALSE(bitset[1] == bitset[2]);

		SECTION("Unequality operator works too") {
			REQUIRE(bitset[0] != bitset[2]);
			REQUIRE(bitset[1] != bitset[2]);
		}
	}
	SECTION("Bitsets with different capacities") {
		REQUIRE(bitset[0] != bitset[3]);
		REQUIRE(bitset[0].capacity() != bitset[3].capacity());

		SECTION("Switched works too") {
			REQUIRE(bitset[3] != bitset[0]);
		}
		SECTION("Equal bitsets with different capacities") {
			bitset[3].set(0, INDEX_LARGE);
			REQUIRE(bitset[0] == bitset[3]);
			REQUIRE(bitset[0].capacity() != bitset[3].capacity());

			SECTION("Switched works too") {
				REQUIRE(bitset[3] == bitset[0]);
			}
		}
	}
}
TEST_CASE("Bitset AND and OR operations works", "[ecs][bitset][and][or]") {
	Bitset bitset[6];

	for(int i = 0; i < 10; i++) {
		bitset[0].set(1, i * 4);
		bitset[3].set(1, i * 4);
		bitset[5].set(1, i * 4);

		bitset[1].set(1, i * 2);
		bitset[3].set(1, i * 2);
	}
	for(int i = 0; i < 5; i++) {
		bitset[2].set(1, i * 4);
	}
	bitset[4].set(1, 100);
	bitset[5].set(1, 100);

	SECTION("AND operator works") {
		REQUIRE((bitset[0] & bitset[1]) == bitset[2]);

		SECTION("Switched works too") {
			REQUIRE((bitset[1] & bitset[0]) == bitset[2]);
		}
		SECTION("Works for large bitsets") {
			REQUIRE((bitset[0] & bitset[4]) == Bitset());
		}
		SECTION("Bitset is empty if no bits are common") {
			Bitset result = bitset[0] & bitset[4];
			REQUIRE(result == Bitset());
			REQUIRE(result.size() == 0);
		}
		SECTION("Bitset is empty if AND with empty") {
			REQUIRE((bitset[0] & Bitset()) == Bitset());
		}
	}
	SECTION("OR operator works") {
		REQUIRE((bitset[0] | bitset[1]) == bitset[3]);

		SECTION("Switched works too") {
			REQUIRE((bitset[1] | bitset[0]) == bitset[3]);
		}
		SECTION("Works for large bitsets") {
			REQUIRE((bitset[0] | bitset[4]) == bitset[5]);
		}
	}
}
