/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "catch.hpp"
#include "ecs.h"

using namespace ecs;

struct TestInt {
	int i;
	TestInt(int i) : i(i) {}
};
struct TestFloat {
	float f;
	TestFloat(float f) : f(f) {}
};

TEST_CASE("Entities and components are managed correctly", "[ecs][entity][component]") {
	World world;
	Entity e = world.create();
	SECTION("Entity validation are working") {
		SECTION("Created entity is valid") {
			REQUIRE(e.valid());

			SECTION("Destroyed entity is invalid") {
				e.destroy();
				REQUIRE_FALSE(e.valid());

				SECTION("Entity id's are reused") {
					Entity e2 = world.create();
					REQUIRE(e == e2);
				}
			}
		}
		SECTION("Not created entity") {
			Entity e2;
			SECTION("is invalid") {
				REQUIRE_FALSE(e2.valid());
			}
			SECTION("is not equal to created entity") {
				REQUIRE(e2 != e);
			}
		}
	}
	SECTION("Equality operations are working") {
		SECTION("Same entity are equal") {
			Entity e2 = e;
			REQUIRE(e2 == e);
			REQUIRE_FALSE(e2 != e);
		}
		SECTION("Separate entities are not equal") {
			Entity e2 = world.create();
			REQUIRE(e2 != e);
			REQUIRE_FALSE(e2 == e);
		}
	}
	SECTION("Entity holds components") {
		int ti = rand();
		Entity er = e.set(TestInt(ti));

		SECTION("set calls can be chained") {
			REQUIRE(er == e);
		}
		SECTION("Component equals set component") {
			REQUIRE(e.get<TestInt>().i == ti);
		}
		SECTION("Entity has component") {
			int ti2 = rand();
			REQUIRE(e.has<TestInt>());

			SECTION("Entity does not have unset component") {
				er = e.unset<TestInt>();
				REQUIRE_FALSE(e.has<TestInt>());

				SECTION("unset calls can be chained") {
					REQUIRE(er == e);
				}
				SECTION("Entity has reset component") {
					e.set<TestInt>(TestInt(ti2));
					REQUIRE(e.has<TestInt>());
					REQUIRE(e.get<TestInt>().i == ti2);
				}
			}
			SECTION("Entity has reset component") {
				e.set<TestInt>(TestInt(ti2));
				REQUIRE(e.has<TestInt>());
				REQUIRE(e.get<TestInt>().i == ti2);
			}
		}
		SECTION("Entity does not have second component") {
			REQUIRE_FALSE(e.has<TestFloat>());
		}
		SECTION("Entity holds second component") {
			float tf = (float)rand() / 100.0f;
			e.set(TestFloat(tf));

			SECTION("Components equals set components") {
				REQUIRE(e.get<TestFloat>().f == tf);
				REQUIRE(e.get<TestInt>().i == ti);
			}
			SECTION("Entity has both components") {
				REQUIRE(e.has<TestFloat>());
				REQUIRE(e.has<TestInt>());
			}
		}
	}
	SECTION("Entity doesn't have any components as default") {
		REQUIRE_FALSE(e.has<TestInt>());
		REQUIRE_FALSE(e.has<TestFloat>());
	}
}
TEST_CASE("Entity creation are managed correctly", "[ecs][entity]") {
	const int ENTITY_COUNT = 20;
	#define INDEX_LOOP for(int i = 0; i < ENTITY_COUNT; i++)
	#define INDEX_LOOP_2 for(int i = 0; i < ENTITY_COUNT; i += 2)
	World world;

	SECTION("Stack allocated entity buffer") {
		Entity e[ENTITY_COUNT];
		world.create(ENTITY_COUNT, e);

		INDEX_LOOP {
			REQUIRE(e[i].valid());
		}
		SECTION("Destroyed entities are invalid") {
			INDEX_LOOP_2 {
				e[i].destroy();
			}
			INDEX_LOOP {
				REQUIRE(e[i].valid() == (i % 2 != 0));
			}
			SECTION("Destroyed entitites are reused") {
				Entity e2[ENTITY_COUNT / 2];
				world.create(sizeof(e2) / sizeof(e2[0]), e2);

				INDEX_LOOP {
					REQUIRE(e[i].valid());
				}
				INDEX_LOOP_2 {
					REQUIRE(e[i] == e2[i / 2]);
				}
			}
		}
	}
	SECTION("Heap allocated entity buffer") {
		Entity *e = world.create(ENTITY_COUNT);
		bool allValid = true;

		INDEX_LOOP {
			if(!e[i].valid()) {
				allValid = false;
			}
		}
		delete[] e;
		REQUIRE(allValid);
	}
	#undef INDEX_LOOP
	#undef INDEX_LOOP_2
}

class TestSystem : public System {
	public:
		int i;
		TestSystem();
	private:
		void update(Entity);
};
class TestSystemFloatReq : public TestSystem {
	public:
		TestSystemFloatReq();
};
class TestSystemFloatExc : public TestSystem {
	public:
		TestSystemFloatExc();
};

const int TEST_SYSTEM_NUM = 5;
TestSystem::TestSystem() : i(TEST_SYSTEM_NUM) {
	require<TestInt>();
}
void TestSystem::update(Entity e) {
	TestInt &testInt = e.get<TestInt>();
	testInt.i = i;
}
TestSystemFloatReq::TestSystemFloatReq() {
	require<TestFloat>();
	i += 3;
}
TestSystemFloatExc::TestSystemFloatExc() {
	exclude<TestFloat>();
	i += 4;
}

TEST_CASE("Systems are managed correctly", "[ecs][system]") {
	World world;
	TestSystem testSystem;
	TestSystemFloatReq testSystemFloatReq;
	TestSystemFloatExc testSystemFloatExc;

	testSystem.init(world);
	testSystemFloatReq.init(world);
	testSystemFloatExc.init(world);

	SECTION("System holds world") {
		REQUIRE(testSystem.getWorld() == &world);
	}
	SECTION("System updates entities") {
		Entity e = world.create();
		e.set(TestInt(1));

		SECTION("System updates entity with component") {
			testSystem.process();
			REQUIRE(e.get<TestInt>().i == testSystem.i);
		}
		SECTION("System updates entity with extra component") {
			e.set(TestFloat(2.5f));
			testSystem.i += 2;
			testSystem.process();
			REQUIRE(e.get<TestInt>().i == testSystem.i);
		}
		SECTION("System updates with two entities") {
			Entity e2 = world.create();
			e2.set(TestInt(2));
			e2.set(TestFloat(3.5f));

			SECTION("System doesn't update entity without required component") {
				testSystemFloatReq.process();

				REQUIRE(e.get<TestInt>().i == 1);
				REQUIRE(e2.get<TestInt>().i == testSystemFloatReq.i);

				SECTION("System updates entity with set component") {
					e.set(TestFloat(4.5f));
					testSystemFloatReq.process();

					REQUIRE(e.get<TestInt>().i == testSystemFloatReq.i);
					REQUIRE(e2.get<TestInt>().i == testSystemFloatReq.i);
				}
			}
			SECTION("System doesn't update entity with excluded component") {
				testSystemFloatExc.process();

				REQUIRE(e.get<TestInt>().i == testSystemFloatExc.i);
				REQUIRE(e2.get<TestInt>().i == 2);
			}
		}
		SECTION("System doesn't update destroyed entity") {
			e.destroy();
			testSystem.process();
			REQUIRE(e.get<TestInt>().i == 1);
		}
	}
	SECTION("System updates many entities") {
		const int ENTITY_COUNT = 10;
		Entity e[ENTITY_COUNT];

		for(int i = 0; i < ENTITY_COUNT; i++) {
			e[i] = world.create();
			e[i].set(TestInt(0));
		}
		testSystem.process();

		for(int i = 0; i < ENTITY_COUNT; i++) {
			REQUIRE(e[i].get<TestInt>().i == testSystem.i);
		}
	}
}

class TestSystemBeginEnd : public System {
	public:
		int nums[3];
		TestSystemBeginEnd();
	private:
		void begin();
		void end();
		void update(Entity);
};
TestSystemBeginEnd::TestSystemBeginEnd() {
	nums[0] = 1;
	nums[1] = 2;
	nums[2] = 3;
	require<TestInt>();
}
void TestSystemBeginEnd::begin() {
	nums[0] += 5;
}
void TestSystemBeginEnd::end() {
	nums[1] += 6;
}
void TestSystemBeginEnd::update(Entity e) {
	nums[2] += e.get<TestInt>().i;
}

TEST_CASE("Begin and end system methods works", "[ecs][system][begin_end]") {
	World world;

	TestSystemBeginEnd system;
	system.init(world);

	REQUIRE(system.nums[0] == 1);
	REQUIRE(system.nums[1] == 2);
	REQUIRE(system.nums[2] == 3);

	Entity e = world.create();
	e.set(TestInt(7));
	system.process();

	REQUIRE(system.nums[0] == 6);
	REQUIRE(system.nums[1] == 8);
	REQUIRE(system.nums[2] == 10);
}

class TestSystemIncr : public System {
	public:
		TestSystemIncr();
	private:
		void update(Entity);
};
TestSystemIncr::TestSystemIncr() {
	require<TestInt>();
}
const int TEST_SYSTEM_INCR_NUM = 2;
void TestSystemIncr::update(Entity e) {
	TestInt &i = e.get<TestInt>();
	i.i += TEST_SYSTEM_INCR_NUM;
}

TEST_CASE("World contained system are managed correctly", "[ecs][system]") {
	World world;
	world.add<TestSystem>();

	SECTION("System updates entities") {
		Entity e = world.create();
		e.set(TestInt(1));

		SECTION("System updates entity with component") {
			world.processAll();
			REQUIRE(e.get<TestInt>().i == TEST_SYSTEM_NUM);
		}
		SECTION("Two systems updates entity") {
			world.add<TestSystemIncr>();
			world.processAll();
			REQUIRE(e.get<TestInt>().i == TEST_SYSTEM_NUM + TEST_SYSTEM_INCR_NUM);
		}
	}
}

class TestSystemGlobal : public System {
	public:
		TestSystemGlobal();
	private:
		void update(Entity);
};
TestSystemGlobal::TestSystemGlobal() {
	require<TestInt>();
}
void TestSystemGlobal::update(Entity e) {
	TestInt &testInt = e.get<TestInt>();
	testInt = global().get<TestInt>();
}

TEST_CASE("Global entity are managed correctly", "[ecs][entity][global]") {
	World world;

	SECTION("Global entity is valid") {
		REQUIRE(world.global().valid());
	}
	SECTION("Global entity holds component") {
		world.global().set(TestInt(2));
		REQUIRE(world.global().has<TestInt>());
		REQUIRE(world.global().get<TestInt>().i == 2);

		SECTION("Global entity holds second component") {
			world.global().set(TestFloat(5.5f));
			REQUIRE(world.global().has<TestFloat>());
			REQUIRE(world.global().get<TestFloat>().f == 5.5f);
		}
		SECTION("System processes global entity") {
			world.add<TestSystem>();
			world.processAll();
			REQUIRE(world.global().get<TestInt>().i == TEST_SYSTEM_NUM);
		}
		SECTION("Global is accessible through entity variable") {
			Entity e = world.global();
			REQUIRE(e.has<TestInt>());
			REQUIRE(e.get<TestInt>().i == 2);

			SECTION("Component can be set through variable") {
				e.set(TestFloat(-6.5f));
				REQUIRE(world.global().has<TestFloat>());
				REQUIRE(world.global().get<TestFloat>().f == -6.5f);
			}
		}
		SECTION("Global is accessible through system") {
			Entity e = world.create();
			e.set(TestInt(-1));

			world.add<TestSystemGlobal>();
			world.processAll();
			REQUIRE(e.get<TestInt>().i == 2);
		}
	}
}

struct TestVelocity {
	int v;
	TestVelocity(int v) : v(v) {}
};
struct TestBounce {
	int wall;
	TestBounce(int wall) : wall(wall) {}
};

class TestMover : public System {
	public:
		TestMover();
	private:
		void update(Entity);
};
class TestBouncer : public System {
	public:
		TestBouncer();
	private:
		void update(Entity);
};
TestMover::TestMover() {
	require<TestInt>();
	require<TestVelocity>();
}
void TestMover::update(Entity e) {
	TestInt &i = e.get<TestInt>();
	TestVelocity &v = e.get<TestVelocity>();
	i.i += v.v;
}
TestBouncer::TestBouncer() {
	require<TestInt>();
	require<TestVelocity>();
	require<TestBounce>();
}
void TestBouncer::update(Entity e) {
	TestInt &i = e.get<TestInt>();
	TestVelocity &v = e.get<TestVelocity>();
	TestBounce &bouncer = e.get<TestBounce>();

	if(i.i >= bouncer.wall) {
		i.i = bouncer.wall;
		v.v *= -1;
	}
}

class TestIntRegister : public System {
	public:
		std::vector<int> ints;
		TestIntRegister();
	private:
		void update(Entity);
};
TestIntRegister::TestIntRegister() {
	require<TestInt>();
}
void TestIntRegister::update(Entity e) {
	ints.push_back(e.get<TestInt>().i);
}

class TestEntityRemover : public System {
	public:
		int limit;
		TestEntityRemover();
	private:
		void update(Entity);
};
TestEntityRemover::TestEntityRemover() {
	require<TestInt>();
}
void TestEntityRemover::update(Entity e) {
	TestInt i = e.get<TestInt>();
	if(i.i >= limit) {
		e.destroy();
	}
}

TEST_CASE("System handles larger move based case", "[ecs][system][move]") {
	World world;
	TestMover mover;
	TestIntRegister intRegister;
	TestBouncer bouncer;
	TestEntityRemover remover;

	mover.init(world);
	intRegister.init(world);
	bouncer.init(world);
	remover.init(world);

	SECTION("First entity") {
		Entity e = world.create();
		e.set(TestInt(9));
		e.set(TestVelocity(2));

		SECTION("Entity moves once") {
			mover.process();
			REQUIRE(e.get<TestInt>().i == 11);

			SECTION("Entity moves a second time") {
				mover.process();
				REQUIRE(e.get<TestInt>().i == 13);
			}
		}
		SECTION("Second entity") {
			Entity e2 = world.create();
			e2.set(TestInt(7));
			e2.set(TestVelocity(3));

			SECTION("Second entity moves too") {
				mover.process();
				REQUIRE(e.get<TestInt>().i == 11);
				REQUIRE(e2.get<TestInt>().i == 10);

				SECTION("One entity bounces while the other don't") {
					e.set(TestBounce(10));
					bouncer.process();
					mover.process();

					REQUIRE(e.get<TestInt>().i == 8);
					REQUIRE(e2.get<TestInt>().i == 13);
				}
			}
		}
	}
	SECTION("Many entities") {
		const int ENTITY_COUNT = 100;
		const int RUNS = 100;

		Entity e[ENTITY_COUNT];
		world.create(ENTITY_COUNT, e);

		for(int i = 0; i < ENTITY_COUNT; i++) {
			e[i].set(TestInt(i));
			e[i].set(TestVelocity(i * 2));
		}
		for(int i = 1; i <= RUNS; i++) {
			mover.process();
			intRegister.process();

			for(unsigned int j = 0; j < intRegister.ints.size(); j++) {
				REQUIRE(intRegister.ints[j] == j + (j * 2) * i);
			}
			intRegister.ints.clear();
		}
	}
	SECTION("Many entities with entity removal") {
		const int RUNS = 50;
		remover.limit = 20;

		for(int r = 0; r < RUNS; r++) {
			mover.process();
			Entity e = world.create();
			e.set(TestInt(0));
			e.set(TestVelocity(1));
			intRegister.process();
			remover.process();

			Bitset ints;
			for(unsigned int i = 0; i < intRegister.ints.size(); i++) {
				ints[intRegister.ints[i]] = 1;
			}
			for(int i = 0; i <= std::min(r, remover.limit); i++) {
				REQUIRE(ints[i]);
			}
			REQUIRE_FALSE(ints[remover.limit + 1]);
			intRegister.ints.clear();
		}
	}
}
