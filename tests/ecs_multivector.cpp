/*
Copyright (c) 2016-2018 Samuel Utbult

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "catch.hpp"
#include "ecs_multivector.h"
#include <string.h>

using namespace ecs;

#define REQUIRE_SIZE(x, y)\
	REQUIRE(vector.getXSize() == (x));\
	REQUIRE(vector.getYSize() == (y));\
	REQUIRE(vector.getXCapacity() >= (x));\
	REQUIRE(vector.getYCapacity() >= (y));

TEST_CASE("Multivector holds values", "[ecs][multivector]") {
	Multivector vector;
	int a;
	SECTION("First index default is zero") {
		REQUIRE(vector.get(0, 0) == 0);

		SECTION("Second x index default is zero") {
			REQUIRE(vector.get(1, 0) == 0);
		}
		SECTION("Second y index default is zero") {
			REQUIRE(vector.get(0, 1) == 0);
		}
	}
	SECTION("First index works") {
		vector.set(&a, 0, 0);
		REQUIRE(vector.get(0, 0) == &a);
		REQUIRE_SIZE(1, 1);

		SECTION("Second x index works") {
			int b;
			vector.set(&b, 1, 0);
			REQUIRE(vector.get(1, 0) == &b);
			REQUIRE(vector.get(0, 0) == &a);
			REQUIRE_SIZE(2, 1);

			SECTION("Second x and y index works") {
				int c;
				vector.set(&c, 1, 1);
				REQUIRE(vector.get(1, 1) == &c);
				REQUIRE(vector.get(1, 0) == &b);
				REQUIRE(vector.get(0, 0) == &a);
				REQUIRE(vector.get(0, 1) == 0);
				REQUIRE_SIZE(2, 2);
			}
		}
		SECTION("Second y index works") {
			int b;
			vector.set(&b, 0, 1);
			REQUIRE(vector.get(0, 1) == &b);
			REQUIRE(vector.get(0, 0) == &a);
			REQUIRE_SIZE(1, 2);
		}
	}
	SECTION("Larger index works") {
		vector.set(&a, 10, 7);
		REQUIRE(vector.get(10, 7) == &a);
		REQUIRE_SIZE(11, 8);
	}
	SECTION("Larger random index works") {
		unsigned int x = rand() % 100;
		unsigned int y = rand() % 100;
		vector.set(&x, x, y);
		REQUIRE(vector.get(x, y) == &x);
		REQUIRE_SIZE(x + 1, y + 1);
	}
	SECTION("Large set of indices in a pattern works") {
		const unsigned int X_START = 50;
		const unsigned int Y_START = 70;
		const unsigned int X_END = 150;
		const unsigned int Y_END = 220;
		#define VALUE (&a + (x + y * X_END * 2))

		for(unsigned int x = X_START; x < X_END; x++) {
			for(unsigned int y = Y_START; y < Y_END; y++) {
				vector.set(VALUE, x, y);
			}
		}
		REQUIRE_SIZE(X_END, Y_END);

		for(unsigned int x = X_START; x < X_END; x++) {
			for(unsigned int y = Y_START; y < Y_END; y++) {
				REQUIRE(vector.get(x, y) == VALUE);
			}
		}
		#undef VALUE
	}
	SECTION("Large set of indices in random locations works") {
		const unsigned int X_LIMIT = 100;
		const unsigned int Y_LIMIT = X_LIMIT;
		const unsigned int COUNT = 1000;
		#define HAS_VALUE_INDEX (x + y * X_LIMIT)
		#define VALUE (&a + (HAS_VALUE_INDEX % 500))

		unsigned int xMax = 0;
		unsigned int yMax = 0;
		bool hasValue[X_LIMIT * Y_LIMIT];
		memset(hasValue, 0, sizeof(hasValue));
		for(unsigned int i = 0; i < COUNT; i++) {
			unsigned int x;
			unsigned int y;
			do {
				x = rand() % X_LIMIT;
				y = rand() % Y_LIMIT;
			} while(hasValue[HAS_VALUE_INDEX]);

			hasValue[HAS_VALUE_INDEX] = 1;
			vector.set(VALUE, x, y);
			xMax = std::max(x, xMax);
			yMax = std::max(y, yMax);
		}
		REQUIRE_SIZE(xMax + 1, yMax + 1);

		for(unsigned int x = 0; x < X_LIMIT; x++) {
			for(unsigned int y = 0; y < Y_LIMIT; y++) {
				void *expected = hasValue[HAS_VALUE_INDEX] ? VALUE : 0;
				REQUIRE(vector.get(x, y) == expected);
			}
		}
		#undef HAS_VALUE_INDEX
		#undef VALUE
	}
}
